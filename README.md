# javartm library

The javartm library allows Java/JVM users to use the "Restricted Transactional Memory" support available on some Intel CPUs. See [here][inteltsx] for more details.

In practice, this allows code running on the JVM to enjoy the benefits of Hardware Transactional Memory.

javartm is distributed under the terms of the GNU LGPLv3; see the COPYING and COPYING.LESSER files for more details.

## Hardware Requirements

As of this writing only CPUs based on the Intel Haswell architecture include the necessary hardware for RTM. See [here][inteltsx] for Intel's announcement, and [here][tsxspecs] for the specifications.

Testing on other CPUs can be done using the [Intel Software Development Emulator][intelsde].

## Software Requirements

### Important: javartm was tested as working with Oracle's JDK 6. Support for OpenJDK >= 7 is missing, and unless there is interest I'll probably not bother updating this project.

## Building

Building is done with ant.
Gcc's native suport for RTM (for versions >= 4.8) is used if available; otherwise javartm includes a fallback alternative.

# Download

Prebuilt packages can be found [here](https://glcdn.githack.com/ivoanjo/javartm/raw/gh-pages/bin-releases/index.html).

## Testing with Intel SDE

The script `scripts/runpin.sh` can be used to run applications with javartm using the Intel SDE, which is supported on all x86-64 CPUs.

## Who am I

See <https://ivoanjo.me>.

Thanks for reading this far! :)

[inteltsx]: http://software.intel.com/en-us/blogs/2012/02/07/transactional-synchronization-in-haswell
[intelsde]: http://software.intel.com/en-us/articles/intel-software-development-emulator
[tsxspecs]: http://software.intel.com/sites/default/files/m/9/2/3/41604 "Intel Architecture Instruction Set Extensions Programming Reference"
